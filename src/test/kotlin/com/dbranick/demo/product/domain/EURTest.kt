package com.dbranick.demo.product.domain

import assertk.all
import assertk.assertThat
import assertk.assertions.*
import org.junit.jupiter.api.Test
import java.math.BigDecimal

internal class EURTest {

	@Test
	fun `should throw exception when value is less than zero`() {
		//given
		val value = "-0.1".toBigDecimal()

		//when + then
		assertThat { EUR(value) }
			.isFailure()
			.isInstanceOf(EURMustBeGreaterOrEqualZeroException::class)
	}

	@Test
	fun `should create object when value is equal zero`() {
		//given
		val value = BigDecimal.ZERO

		//when + then
		assertThat { EUR(value) }
			.isSuccess()
			.all { prop(EUR::value).isEqualTo(BigDecimal.ZERO) }
	}

	@Test
	fun `should create object when value is greater than zero`() {
		//given
		val value = BigDecimal.TEN

		//when + then
		assertThat { EUR(value) }
			.isSuccess()
			.all { prop(EUR::value).isEqualTo(BigDecimal.TEN) }
	}

}
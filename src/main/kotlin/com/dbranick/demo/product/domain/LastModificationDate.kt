package com.dbranick.demo.product.domain

import com.fasterxml.jackson.annotation.JsonValue
import java.time.LocalDateTime
import java.time.ZoneOffset

internal data class LastModificationDate(
	@JsonValue val value: LocalDateTime
) {

	companion object {
		fun now() = LastModificationDate(LocalDateTime.now(ZoneOffset.UTC))
	}
}
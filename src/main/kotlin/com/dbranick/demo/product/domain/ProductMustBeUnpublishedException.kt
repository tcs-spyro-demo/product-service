package com.dbranick.demo.product.domain

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.BAD_REQUEST, reason = "Product must be unpublished")
internal class ProductMustBeUnpublishedException : Exception("Product must be unpublished")
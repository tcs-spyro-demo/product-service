package com.dbranick.demo.product.domain

import com.fasterxml.jackson.annotation.JsonValue
import java.util.*

internal data class ProductId(
	@JsonValue val value: String
) {
	constructor() : this(UUID.randomUUID().toString())
	constructor(value: UUID) : this(value.toString())
}
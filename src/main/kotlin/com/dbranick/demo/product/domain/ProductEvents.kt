package com.dbranick.demo.product.domain

import java.io.Serializable
import java.math.BigDecimal
import java.time.LocalDateTime

internal interface ProductEvent : Serializable {
	val productId: String
}

internal class ProductUnpublished(
	override val productId: String,
) : ProductEvent


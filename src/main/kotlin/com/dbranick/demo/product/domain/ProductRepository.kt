package com.dbranick.demo.product.domain

import org.springframework.data.domain.PageImpl
import org.springframework.stereotype.Repository

@Repository
internal interface ProductRepository {
	fun save(product: Product)
	fun findProduct(productId: ProductId): Product?
	fun getAllPublishedProducts(page: Int, size: Int): PageImpl<Product>
	fun remove(product: Product)
}
package com.dbranick.demo.product.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document


@Document("products")
internal class Product private constructor(
	val name: ProductName,
	val sku: SKU,
	description: ProductDescription,
	price: EUR
) {
	@Id
	var id: ProductId = ProductId()
		private set
	var price: EUR = price
		private set
	var description: ProductDescription = description
		private set
	var lastModificationDate: LastModificationDate = LastModificationDate.now()
		private set
	var isPublished: Boolean = false
		private set

	fun changePrice(newPrice: EUR) = apply {
		price = newPrice
		lastModificationDate = LastModificationDate.now()
	}

	fun changeDescription(newDescription: ProductDescription) = apply {
		description = newDescription
		lastModificationDate = LastModificationDate.now()
	}

	fun publish() = apply {
		isPublished = true
		lastModificationDate = LastModificationDate.now()
	}

	fun unpublish() = apply {
		isPublished = false
		lastModificationDate = LastModificationDate.now()
	}

	companion object {
		fun new(
			name: ProductName,
			sku: SKU,
			description: ProductDescription,
			price: EUR
		) = Product(name, sku, description, price)
	}
}

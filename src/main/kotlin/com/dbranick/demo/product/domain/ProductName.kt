package com.dbranick.demo.product.domain

import com.dbranick.demo.product.utils.validate
import com.fasterxml.jackson.annotation.JsonValue
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

internal data class ProductName(@JsonValue val value: String) {
	init {
		validate(value.isNotBlank()) { ProductNameIsBlankException() }
	}
}

@ResponseStatus(HttpStatus.BAD_REQUEST, reason = "Product name must be not blank")
internal class ProductNameIsBlankException : Exception("Product name must be not blank")

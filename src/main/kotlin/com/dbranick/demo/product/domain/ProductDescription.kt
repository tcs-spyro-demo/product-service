package com.dbranick.demo.product.domain

import com.dbranick.demo.product.utils.validate
import com.fasterxml.jackson.annotation.JsonValue
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

internal data class ProductDescription(@JsonValue val value: String) {
	init {
		validate(value.isNotBlank()) { ProductDescriptionBlankException() }
	}
}

@ResponseStatus(HttpStatus.BAD_REQUEST, reason = "Product description must be not blank")
internal class ProductDescriptionBlankException : Exception("Product description must be not blank")

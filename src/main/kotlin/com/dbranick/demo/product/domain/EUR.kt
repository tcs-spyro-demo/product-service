package com.dbranick.demo.product.domain

import com.dbranick.demo.product.utils.validate
import com.fasterxml.jackson.annotation.JsonValue
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
import java.math.BigDecimal

internal data class EUR(
	@JsonValue val value: BigDecimal
) {
	init {
		validate(value >= BigDecimal.ZERO) { EURMustBeGreaterOrEqualZeroException(value) }
	}
}

@ResponseStatus(HttpStatus.BAD_REQUEST, reason = "EUR value must be greater or equal zero")
internal class EURMustBeGreaterOrEqualZeroException(value: BigDecimal)
	: Exception("EUR value must be greater or equal zero but is $value")
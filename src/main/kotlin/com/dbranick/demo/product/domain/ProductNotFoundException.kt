package com.dbranick.demo.product.domain

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.NOT_FOUND, reason = "Product not found")
internal class ProductNotFoundException(productId: ProductId) : Exception("Product ${productId.value} not found")
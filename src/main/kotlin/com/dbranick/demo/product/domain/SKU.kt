package com.dbranick.demo.product.domain

import com.dbranick.demo.product.utils.validate
import com.fasterxml.jackson.annotation.JsonValue
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

internal data class SKU(
	@JsonValue val value: String
) {
	init {
		validate(value.isNotBlank()) { SKUMustBeNotBlankException() }
	}
}

@ResponseStatus(HttpStatus.BAD_REQUEST)
internal class SKUMustBeNotBlankException : Exception("SKU must be not blank")
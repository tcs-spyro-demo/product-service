package com.dbranick.demo.product.application

import java.math.BigDecimal

internal interface EditProductCommand {
	val description: String
	val price: BigDecimal
}

internal interface CreateProductCommand {
	val sku: String
	val name: String
	val description: String
	val price: BigDecimal
}
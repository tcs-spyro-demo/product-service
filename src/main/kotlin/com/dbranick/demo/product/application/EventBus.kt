package com.dbranick.demo.product.application

import com.dbranick.demo.product.domain.ProductEvent

internal interface EventBus {
	fun publish(event: ProductEvent)
}
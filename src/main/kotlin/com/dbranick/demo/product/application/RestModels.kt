package com.dbranick.demo.product.application

import java.math.BigDecimal
import java.time.LocalDateTime
import java.util.*

internal class ProductRestDTO(
	val id: UUID,
	val name: String,
	val description: String,
	val sku: String,
	val price: BigDecimal,
	val lastModificationDate: LocalDateTime,
	val isPublished: Boolean
)

internal class ProductPageRestDTO(
	val items: List<ProductRestDTO>,
	val totalItems: Long,
	val totalPages: Int,
)
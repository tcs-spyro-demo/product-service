package com.dbranick.demo.product.application

import com.dbranick.demo.product.domain.*
import com.dbranick.demo.product.utils.validate
import org.springframework.stereotype.Component
import java.util.*

@Component
internal class ProductFacade(
	private val productRepository: ProductRepository,
	private val eventBus: EventBus
) {

	fun createProduct(command: CreateProductCommand): String {
		val product = Product.new(
			description = ProductDescription(command.description),
			name = ProductName(command.name),
			price = EUR(command.price),
			sku = SKU(command.sku)
		)
		productRepository.save(product)
		return product.id.value
	}

	fun editProduct(id: UUID, command: EditProductCommand) {
		updateProduct(id) {
			it.changeDescription(ProductDescription(command.description))
				.changePrice(EUR(command.price))
		}
	}

	fun publishProduct(id: UUID) {
		updateProduct(id) {
			it.publish()
		}
	}

	fun unpublishProduct(id: UUID) {
		updateProduct(id) {
			it.unpublish()
		}.also {
			eventBus.publish(ProductUnpublished(it.id.value))
		}
	}

	fun getProduct(id: UUID): ProductRestDTO {
		val productId = ProductId(id)
		val product = productRepository.findProduct(productId)
			?: throw ProductNotFoundException(productId)

		return product.toProductRestView()
	}

	fun getAllPublishedProducts(page: Int, size: Int) : ProductPageRestDTO {
		val result = productRepository.getAllPublishedProducts(page, size)
		return with(result) {
			ProductPageRestDTO(
				items = content.map { it.toProductRestView() },
				totalItems = totalElements,
				totalPages = totalPages
			)
		}
	}

	private fun Product.toProductRestView() = ProductRestDTO(
		id = UUID.fromString(id.value),
		lastModificationDate = lastModificationDate.value,
		price = price.value,
		name = name.value,
		description = description.value,
		isPublished = isPublished,
		sku = sku.value
	)

	private fun updateProduct(id: UUID, change: (Product) -> Product): Product {
		val productId = ProductId(id)
		val product = productRepository.findProduct(productId)
			?: throw ProductNotFoundException(productId)

		val modifiedProduct = change(product)
		productRepository.save(modifiedProduct)
		return modifiedProduct
	}

	fun removeProduct(id: UUID) {
		val productId = ProductId(id)
		val product = productRepository.findProduct(productId)
			?: throw ProductNotFoundException(productId)

		validate(product.isPublished) {
			ProductMustBeUnpublishedException()
		}

		productRepository.remove(product)
	}
}
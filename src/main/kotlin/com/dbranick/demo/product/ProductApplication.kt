package com.dbranick.demo.product

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication

@SpringBootApplication
@ConfigurationPropertiesScan("com.dbranick.demo.product")
public class ProductApplication

public fun main(args: Array<String>) {
	runApplication<ProductApplication>(*args)
}

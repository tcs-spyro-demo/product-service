package com.dbranick.demo.product.utils

import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.contract

@OptIn(ExperimentalContracts::class)
internal fun validate(condition: Boolean, exception: () -> Exception) {
	contract {
		returns() implies condition
	}

	if (!condition) {
		throw exception()
	}
}


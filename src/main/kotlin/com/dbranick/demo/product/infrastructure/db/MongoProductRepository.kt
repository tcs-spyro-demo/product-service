package com.dbranick.demo.product.infrastructure.db

import com.dbranick.demo.product.domain.Product
import com.dbranick.demo.product.domain.ProductId
import com.dbranick.demo.product.domain.ProductRepository
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.find
import org.springframework.data.mongodb.core.findById
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.stereotype.Component
import kotlin.reflect.KProperty

@Component
internal class MongoProductRepository(
	private val mongoTemplate: MongoTemplate
) : ProductRepository {

	override fun save(product: Product) {
		mongoTemplate.save(product)
	}

	override fun findProduct(productId: ProductId): Product? {
		return mongoTemplate.findById(productId)
	}

	override fun getAllPublishedProducts(page: Int, size: Int): PageImpl<Product> {
		val pageable = PageRequest.of(page, size)
		val query = Query.query(where(Product::isPublished).`is`(true))
		val count = mongoTemplate.count(query, Product::class.java)
		val results: List<Product> = mongoTemplate.find(query.with(pageable))
		return PageImpl(results, pageable, count)
	}

	override fun remove(product: Product) {
		mongoTemplate.remove(product)
	}

}

private fun where(property: KProperty<*>) = Criteria.where(property.name)
package com.dbranick.demo.product.infrastructure.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2
import java.util.function.Predicate

@Configuration
@EnableSwagger2
internal class SwaggerConfiguration {

	@Bean
	fun api(): Docket = Docket(DocumentationType.SWAGGER_2)
		.select()
		.apis(RequestHandlerSelectors.any())
		.paths(Predicate.not(PathSelectors.regex("/api/error.*")))
		.build()
		.apiInfo(apiInfo())

	private fun apiInfo() = ApiInfoBuilder()
		.title("Product service - demo")
		.description("API Documentation")
		.version("1.0.0")
		.build()
}
package com.dbranick.demo.product.infrastructure.configuration

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.util.StdDateFormat
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule
import org.springframework.amqp.core.Binding
import org.springframework.amqp.core.Queue
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory
import org.springframework.amqp.rabbit.connection.ConnectionFactory
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.amqp.core.TopicExchange
import org.springframework.amqp.core.BindingBuilder
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.boot.context.properties.EnableConfigurationProperties


public const val exchangeTopic: String = "c.d.d.p"
public const val routingKey: String = "productEvent"

@Configuration
@EnableConfigurationProperties(RabbitConfig::class)
internal class RabbitConfiguration {

	@Bean
	fun connectionFactory(rabbitConfig: RabbitConfig): ConnectionFactory = CachingConnectionFactory()
		.apply {
			host = rabbitConfig.host
			port = rabbitConfig.port
			username = rabbitConfig.username
			setPassword(rabbitConfig.password)
		}

	@Bean
	fun productsQueue() = Queue("productsQueue")

	@Bean
	fun binding(queue: Queue, exchange: TopicExchange): Binding {
		return BindingBuilder.bind(queue).to(exchange).with(routingKey)
	}

	@Bean
	fun exchange(): TopicExchange = TopicExchange(exchangeTopic)

	@Bean
	fun producerJackson2MessageConverter(): Jackson2JsonMessageConverter =
		Jackson2JsonMessageConverter(objectMapper())

	@Bean
	fun rabbitTemplate(connectionFactory: ConnectionFactory) = RabbitTemplate(connectionFactory)
		.apply {
			messageConverter = producerJackson2MessageConverter()
		}

	private fun objectMapper(): ObjectMapper {
		val mapper = ObjectMapper()
			.registerModule(ParameterNamesModule())
			.registerModule(Jdk8Module())
			.registerModule(JavaTimeModule())
			.registerModule(KotlinModule())
		mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
		mapper.dateFormat = StdDateFormat()
		return mapper
	}
}

@ConstructorBinding
@ConfigurationProperties(prefix = "demo.rabbit")
internal data class RabbitConfig(
	val host: String = "localhost",
	val port: Int = 5672,
	val username: String = "guest",
	val password: String = "guest"
)
package com.dbranick.demo.product.infrastructure.error

import org.springframework.core.annotation.AnnotatedElementUtils
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.lang.Exception
import java.time.LocalDateTime
import java.time.ZoneOffset

@ControllerAdvice
internal class ErrorHandler : ResponseEntityExceptionHandler() {

	@ExceptionHandler(Exception::class)
	fun handleException(exception: Exception): ResponseEntity<ApiError> {
		val springAnnotation = AnnotatedElementUtils
			.findMergedAnnotation(exception::class.java, ResponseStatus::class.java)
		val timestamp = LocalDateTime.now(ZoneOffset.UTC)
		val status = springAnnotation?.code ?: HttpStatus.INTERNAL_SERVER_ERROR

		val message = when {
			status.is5xxServerError -> "Server error"
			status.is4xxClientError && springAnnotation != null -> springAnnotation.reason
			else -> "${status.value()} Error"
		}

		return ResponseEntity(ApiError(
			message = message.ifBlank { "Unknown error" },
			status = status.value(),
			timestamp = timestamp
		), status)
	}
}

internal class ApiError(
	val message: String,
	val status: Int,
	val timestamp: LocalDateTime
)
package com.dbranick.demo.product.infrastructure

import com.dbranick.demo.product.domain.*
import com.dbranick.demo.product.domain.EUR
import com.dbranick.demo.product.domain.Product
import com.dbranick.demo.product.domain.ProductDescription
import com.dbranick.demo.product.domain.ProductName
import com.dbranick.demo.product.domain.SKU
import com.dbranick.demo.product.utils.logger
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

@Component
internal class DemoProductsGenerator(
	private val productRepository: ProductRepository
) {

	@PostConstruct
	fun generate() {
		repeat(10) {
			val product = Product.new(
				ProductName("Product $it"),
				SKU("product-$it"),
				ProductDescription("Some description"),
				EUR("2.2".toBigDecimal() * it.toBigDecimal())
			)
			product.publish()
			productRepository.save(product)
		}

		logger.info("Generated demo products")
	}
}
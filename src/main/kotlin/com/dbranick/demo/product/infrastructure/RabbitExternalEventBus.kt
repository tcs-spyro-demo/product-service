package com.dbranick.demo.product.infrastructure

import com.dbranick.demo.product.application.EventBus
import com.dbranick.demo.product.domain.ProductEvent
import com.dbranick.demo.product.infrastructure.configuration.exchangeTopic
import com.dbranick.demo.product.infrastructure.configuration.routingKey
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.stereotype.Component

@Component
internal class RabbitExternalEventBus(
	private val rabbitTemplate: RabbitTemplate
) : EventBus {

	override fun publish(event: ProductEvent) {
		rabbitTemplate.convertAndSend(exchangeTopic, routingKey, event)
	}
}
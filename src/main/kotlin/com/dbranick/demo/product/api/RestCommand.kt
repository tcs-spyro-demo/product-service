package com.dbranick.demo.product.api

import com.dbranick.demo.product.application.CreateProductCommand
import com.dbranick.demo.product.application.EditProductCommand
import java.math.BigDecimal

internal class EditProductRestCommand(
	override val description: String,
	override val price: BigDecimal
) : EditProductCommand

internal class CreateProductRestCommand(
	override val name: String,
	override val description: String,
	override val price: BigDecimal,
	override val sku: String
) : CreateProductCommand
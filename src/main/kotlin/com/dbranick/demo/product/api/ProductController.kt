package com.dbranick.demo.product.api

import com.dbranick.demo.product.application.ProductFacade
import com.dbranick.demo.product.application.ProductPageRestDTO
import com.dbranick.demo.product.application.ProductRestDTO
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@Api("Product")
@RestController
internal class ProductController(
	private val productFacade: ProductFacade
) {

	@ApiOperation("Get product")
	@GetMapping("/products/{id}")
	fun getProduct(
		@ApiParam("Product ID", required = true)
		@PathVariable id: UUID
	): ProductRestDTO {
		return productFacade.getProduct(id)
	}

	@ApiOperation("Get published products")
	@GetMapping("/products")
	fun getProducts(
		@ApiParam("Page number (start from 0)", required = true, defaultValue = "0")
		@RequestParam pageNumber: Int,
		@ApiParam("Page elements size", required = true, defaultValue = "10")
		@RequestParam pageSize: Int
	): ProductPageRestDTO {
		return productFacade.getAllPublishedProducts(pageNumber, pageSize)
	}

	@ApiOperation("Publish product")
	@PatchMapping("/products/{id}/publish")
	fun publishProduct(
		@ApiParam("Product ID", required = true)
		@PathVariable id: UUID
	) {
		productFacade.publishProduct(id)
	}

	@ApiOperation("Unpublish product")
	@PatchMapping("/products/{id}/unpublish")
	fun unpublishProduct(
		@ApiParam("Product ID", required = true)
		@PathVariable id: UUID
	) {
		productFacade.unpublishProduct(id)
	}

	@ApiOperation("Create new product")
	@PostMapping("/products")
	fun createProduct(
		@ApiParam("Create product command")
		@RequestBody command: CreateProductRestCommand
	): ResponseEntity<String> =
		ResponseEntity(productFacade.createProduct(command), HttpStatus.CREATED)

	@ApiOperation("Edit product")
	@PutMapping("/products/{id}")
	fun editProduct(
		@ApiParam("Product ID", required = true)
		@PathVariable id: UUID,
		@ApiParam("Edit product command")
		@RequestBody command: EditProductRestCommand
	) {
		productFacade.editProduct(id, command)
	}

	@ApiOperation("Remove product")
	@DeleteMapping("/products/{id}")
	fun removeProduct(
		@ApiParam("Product ID", required = true)
		@PathVariable id: UUID,
	) {
		productFacade.removeProduct(id)
	}
}